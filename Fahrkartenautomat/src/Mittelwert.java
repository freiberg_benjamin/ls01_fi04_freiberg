import java.util.Scanner;

public class Mittelwert {

   public static void main(String[] args) {

      // (E) "Eingabe"
      // Werte für x und y festlegen:
      // ===========================
	  Scanner scan = new Scanner(System.in);
      double x = liesDoubleWertEin("Bitte geben Sie die erste Zahl ein: ");
      double y = liesDoubleWertEin("Bitte geben Sie die zweite Zahl ein: ");
      double m;
      
     m = mittelwertBerechnen(x, y); 
      
      // (A) Ausgabe
      // Ergebnis auf der Konsole ausgeben:
      // =================================
      System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
   }
   
   public static double liesDoubleWertEin(String frage)	{
   double benutzereingabe;
   Scanner scan = new Scanner(System.in);
   System.out.println(frage);
   benutzereingabe = scan.nextDouble();
   return benutzereingabe;
   }
   
// (V) Verarbeitung
   // Mittelwert von x und y berechnen: 
   // ================================
   public static double mittelwertBerechnen(double x, double y)	{
   double m;
   m = (x + y) / 2.0;
		   return m;
   }
}
